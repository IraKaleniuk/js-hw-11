"use strict";

const form = document.querySelector(".password-form");
const passArr = document.querySelectorAll("input");

const error = document.createElement("p");

for (let pass of passArr) {
    pass.addEventListener("copy", (e) => {
        alert("Копіювати пароль не можна!")
        e.preventDefault()
    });
}

form.addEventListener("click", (e) => {
    const el = e.target;
    if(el.tagName === "I") {
        if (el.classList.contains("fa-eye")) {
            el.style.display = "none";
            el.nextElementSibling.style.display = "block";
            el.closest(".input-wrapper").querySelector("input").type = "password";
        } else {
            el.style.display = "none";
            el.previousElementSibling.style.display = "block";
            el.closest(".input-wrapper").querySelector("input").type = "text";
        }
    }
})

form.addEventListener("submit", (e) => {
        e.preventDefault();
        let arr = Array.from(passArr);
        if (arr[0].value === arr[1].value && arr[0].value !== "") {
            error.remove();
            arr.forEach(el => el.value = "");
            setTimeout(() => alert("You are welcome!"), 100);
        } else if (arr[0].value === "" && arr[1].value === "") {
            error.textContent = "Потрібно ввести значення!";
            error.style.cssText = "margin-top:-10px;color:red";
            passArr[1].after(error);
        } else {
            error.textContent = "Потрібно ввести однакові значення!";
            error.style.cssText = "margin-top:-10px;color:red";
            passArr[1].after(error);
        }
})














